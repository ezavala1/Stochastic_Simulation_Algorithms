# Delay Stochastic Simulation Algorithms

An implementation of the Delay Stochastic Simulation Algorithm (DSSA) developed in: https://www.sciencedirect.com/science/article/pii/S0006349513057998

The DSSA reproduces exact trajectories of the chemical master equation for low dosage gene expression. 

The DSSAcd adds cell division features (incl. cell volume changes, gene replication, etc.)