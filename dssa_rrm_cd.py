# -*- coding: utf-8 -*-
"""
Created on Mon Dic 26 11:57:10 2013

@author: zavalaeder
"""

#######################################################################################
#  Delay Stochastic Simulation Algorithm with Cell Division - Reaction Rejection Method 
#######################################################################################

"""
Based on the Reaction Rejection Method described in 
BarrioBurrageBurrageLeierMarquez-Lago 2010 IGI 169-197

Cell division algorithm follows from
LuVolfsonTsimringHasty 2004 SystBiol 1(1) 121-128

USAGE:

- Works for systems where propensities are time-dependent while others are not. For the simpler scenario where all 
  propensities are time-dependent see Case 2 in Lu et al. 2004 SystBiol.
- Time is not normalized to volume.

"""

from numpy.random import uniform, binomial, multinomial, exponential, random
from numpy import arange, zeros, log, vstack, where, exp

from scipy.special import lambertw

class Model:
    def __init__(self,vnames,rates,inits,rxs,pxs,delays,cons,propensity,ind,td,tcc,V0):
        '''
                   vnames: list of strings
                    rates: list of fixed rate parameters
                    inits: list of initial values of variables
                      rxs: reactants matrix
                      pxs: products matrix
                   delays: delays vector
                     cons: consuming reactions info vector
               propensity: list of lambda functions of the form:
                           lambda r,ini: some function of rates and inits.
        '''
        self.vn = vnames
        self.rates = rates
        self.inits = inits
        self.rxs = rxs
        self.pxs = pxs
        self.delays = delays
        self.cons = cons
        self.pv = propensity
        self.ind = ind
        self.td = td
        self.tcc = tcc
        self.V0 = V0
        self.pvl = len(self.pv) # length of propensity vector
        self.nvars = len(self.inits) # number of variables
        self.time = None
        self.series = None
        self.steps = 0
    
    def getStats(self):
        return self.time,self.series,self.steps
    
    def run(self, tmax, reps):
        self.res = zeros((tmax,self.nvars,reps),dtype=float)
#        self.tvec = zeros((tmax,reps),dtype=float)
        tvec = arange(tmax, dtype=int)
        for i in xrange(reps):
            steps = self.DSSA(tmax,i)
#        self.time=self.tvec[0:steps,:]
        self.time=tvec
#        self.series=self.res[0:steps,:,:]
        self.series=self.res
        self.steps=steps
    
    '''DSSA with Cell Division - Reaction Rejection Method'''
    def DSSA(self, tmax, round):

        '''Pass model variables to the algorithm'''
        #(initial conditions, reaction rates, propensities, delays and stoichiometric matrix)
        state = self.inits
        r = self.rates
        pvi = self.pv
        ind = self.ind
        td = self.td
        tcc = self.tcc
        V0 = self.V0
        l = self.pvl
        delays = self.delays
        cons = self.cons
        pv = zeros(l,dtype=float)
        rxs = self.rxs 
        pxs = self.pxs
        
        '''Initialize time vector, steps, state vector, cell division time, stoichiometric matrix and schedule'''
        t = 0
        steps = 0
        self.res[0,:,round] = state
#        self.tvec[0,round] = t
        c = log(2) # a useful constant involved in propensities accounting for cell volume duplication
        next_div_time = tcc
        last_div_time = 0
        gsv = 0  # index of gene/promoter state variable
        stoich = rxs + pxs # stoichiometric matrix
        schedule = zeros((1,2)) # initialize schedule matrix with update time (1st column) and reaction index (2nd column)        

        '''Define auxiliary functions'''
        def search(x): return t < x <= t+tau # function that searches for scheduled reactions within (tc,tc+tau)
        
#        def lambert(x): # Lambert W function using Newton's method
#            eps = 1e-8 # max error allowed
#            w = x
#            while True:
#                ew = exp(w)
#                wNew = w - (w * ew - x) / (w * ew + ew)
#                if abs(w - wNew) <= eps: break
#                w = wNew
#            return w
            
        def volume(x): # Calculates volume increase as a function of time
            V = V0*2**((x - last_div_time)/(next_div_time - last_div_time)) # Exponential growth (very close to linear) as in Lu et al. 2004 SystBiol
            return V

        '''MAIN LOOP'''
        for tim in xrange(1,tmax):
            while t < tim:

                '''Propensities'''               
                As = zeros(1,dtype=float) # volume-dependent propensities
                Aq = zeros(1,dtype=float) # volume-independent propensities
                
                for i in xrange(l): # for each reaction, calculate its propensity according to the current state vector
                    if ind[i] == 1:
                        pv[i] = (pvi[i](r,state))/volume(t) # specify how the volume affects propensities
#                        print 'pv =', pv
                        As += pv[i] # sum of volume-dependent propensities for current state
                    else:
                        pv[i] = pvi[i](r,state)
                        Aq += pv[i] # sum of volume-independent propensities for current state
                
                alpha = As/Aq
                beta = c*log(random())/Aq
#                print 'alpha =', alpha
#                print ' beta =', beta
#                print 'exp(alpha + beta) =', exp(alpha + beta)
                
                '''Random selection of next time step'''
                #tau = (-1/(As+Aq))*log(random())  # time at which next reaction will occur
                tau = (lambertw(alpha*exp(alpha + beta), k=0, tol=1e-8) - alpha - beta)/c  # time at which next reaction will occur
                tau = tau.real

                '''Random selection of next reaction (depending on position during cell cycle)'''                
                if t + tau < next_div_time:
                    
                    if t + tau >= td:   # if time reaches genome replication event
                        state[gsv] = 2*state[gsv]    # gene dosage increase (duplication)
                        td += tcc

                    As = zeros(1,dtype=float)
                    Aq = zeros(1,dtype=float)
                    
                    for i in xrange(l): # for each reaction, calculate its propensity according to the current state vector
                        if ind[i] == 1:
                            '''Cell growth dependence'''
                            pv[i] = (pvi[i](r,state))/volume(t+tau) # specify how the volume affects propensities
                            As += pv[i]
                        else:
                            pv[i] = pvi[i](r,state)
                            Aq += pv[i]

                    mu = zeros(l,dtype=int)
                    mu[where(pv.cumsum() >= (As + Aq)*random())[0][0]] = 1 # reaction event which will happen on this iteration
                    #mu = multinomial(1,pv/(As+Aq)) # reaction event which will happen on this iteration
                else:
                    '''Cell Division Event'''
                    for i in xrange(len(state)):
                        
                        if i == gsv:    # if the state variable is the gene/promoter, then...
                            state[gsv] = state[gsv]/2   # split half the genetic material to daughter cells
                            continue    # continue to next iteration in the for loop
                        
                        if state[i]!=0:  # if not zero number of the i-th molecular species
                            state[i] = binomial(state[i],0.5)  # Partition of molecule numbers to daughter cells drawn from binomial distribution
                    
                    last_div_time = next_div_time # Store record of current division time
                    next_div_time += tcc # Schedule next cell division event
                    t += tau # update time
                    continue # no reaction is executed, continue to next iteration in the while loop
                               
                '''If there is a delay reaction pending, reject drawn reaction and update the delay reaction instead'''
                pending = filter(search,schedule[:,0]) # Search if delay reactions are scheduled within (t,t+tau)
                if pending:    # If there are pending reactions,
                    update_time = min(pending) # find the closest one and save its update time,
                    nr = schedule[where(schedule[:,0]==update_time)[0][0],1] # and save its index too
                    
                    mu = zeros(l,dtype=int) # reject previously drawn reaction mu,
                    mu[nr] = 1  # and replace it with scheduled reaction

                    # If mu is a consuming reaction
                    if cons[mu.nonzero()[0][0]]==1:
                        state += pxs[:,mu.nonzero()[0][0]] # update products
                    else:
                        state += stoich[:,mu.nonzero()[0][0]] # update products and reactants
                    
                    t += update_time - t  # update time to completion of delayed reaction

                else:

                    '''If NO delay reaction is pending, check whether drawn reaction mu is a delay reaction'''
                    if delays[mu.nonzero()[0][0]] == 0: # If drawn reaction mu is NOT a delay-type reaction,
                        state += stoich[:,mu.nonzero()[0][0]] # update state vector immediately                  
                    else:
                        '''Schedule drawn reaction mu if it's a delay-type reaction'''
                        schedule = vstack((schedule,[t+tau+delays[mu.nonzero()[0][0]],mu.nonzero()[0][0]]))
                        
                        # If mu is a consuming reaction
                        if cons[mu.nonzero()[0][0]]==1:
                            state += rxs[:,mu.nonzero()[0][0]] # update reactants

                    t += tau # update time

                steps += 1
                if As + Aq == 0: break
            self.res[tim,:,round] = state
#            self.tvec[tim,round] = t
            if As + Aq == 0: break
            
        return steps
