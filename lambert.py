# -*- coding: utf-8 -*-
"""
Created on Tue Jan  7 08:51:17 2014

@author: zavalaeder
"""

# http://en.wikipedia.org/wiki/Lambert_W_function

import math
eps = 1e-15 # max error allowed
def lambert(x): # Lambert W function using Newton's method
    w = x
    while True:
        ew = math.exp(w)
        wNew = w - (w * ew - x) / (w * ew + ew)
        if abs(w - wNew) <= eps: break
        w = wNew
    return w
